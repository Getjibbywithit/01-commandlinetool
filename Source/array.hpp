//
//  array.hpp
//  CommandLineTool
//
//  Created by lindsey wiltshire on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef array_hpp
#define array_hpp

class Array
{
public:
    Array(); /** a constructor that intialises the data members*/
    
    ~Array(); /** a destructor that deletes the memory allocated to the array*/
    
    void add (float itemValue); /** which adds new items to the end of the array*/
    
    float get (int index); /** which returns the item at the index*/
    

private:
    int size; /** which return the number of items currently in the array*/
    
    
    
};

#endif /* array_hpp */
